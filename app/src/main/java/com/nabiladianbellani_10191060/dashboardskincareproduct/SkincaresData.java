package com.nabiladianbellani_10191060.dashboardskincareproduct;

import java.util.ArrayList;

public class SkincaresData {
    private static String[] datasNames = {
            "Facial Wash",
            "Serum",
            "SunScreen",
            "Pelembab",
    };
    private static String[] datasDetails = {
            "Rp. 80.000,-",
            "Rp. 135.000,-",
            "Rp. 140.000,-",
            "Rp. 90.0000,-"
    };
    private static int[] datasImages = {
            R.drawable.facialwash,
            R.drawable.serum,
            R.drawable.spf,
            R.drawable.pelmbab
    };
    static ArrayList<Skincare> getListData() {
        ArrayList<Skincare> list = new ArrayList<>();
        for (int position = 0; position < datasNames.length; position++) {
            Skincare skincare = new Skincare();
            skincare.setName(datasNames[position]);
            skincare.setDetail(datasDetails[position]);
            skincare.setPhoto(datasImages[position]);
            list.add(skincare);
        }
        return list;
    }
}
