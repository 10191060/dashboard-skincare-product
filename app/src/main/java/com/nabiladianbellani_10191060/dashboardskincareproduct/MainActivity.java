package com.nabiladianbellani_10191060.dashboardskincareproduct;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rvSkincares;
    private ArrayList<Skincare> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvSkincares = findViewById(R.id.rv_datas);
        rvSkincares.setHasFixedSize(true);

        list.addAll(SkincaresData.getListData());
        showRecyclerList();
    }
    private void showRecyclerList() {
        rvSkincares.setLayoutManager(new LinearLayoutManager(this));
        ListSkincareAdapter listHeroAdapter = new ListSkincareAdapter(list);
        rvSkincares.setAdapter(listHeroAdapter);
    }
}